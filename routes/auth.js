const { Router } = require("express");
const auth = require("../middlewares/auth");
const authController = require("../controllers/auth");

const router = Router();

router.post("/api/login", authController.login);
router.get("/api/user", auth, authController.getUser);

module.exports = router;
