const { Router } = require('express');

const todoController = require('../controllers/todos');

const router = Router();

router.get('/api/todos/:page?/:filter?/:search?', todoController.getTodos);
router.post('/api/todos', todoController.postTodo);
router.get('/api/todo/:id', todoController.getTodo);
router.post('/api/todo/:id', todoController.editTodo);



module.exports = router;