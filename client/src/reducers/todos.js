import {
	GET_TODOS,
	ADD_TODO,
	TODO_ERRORS,
	UPDATE_TODO,
	GET_TODO,
	CLEAR_ERRORS
} from "../actions/types";

const initialState = {
	data: [],
	pageSize: 0,
	count: 0,
	todo: null,
	errors: []
};

export default function(state = initialState, action) {
	const { type, payload } = action;

	switch (type) {
		case GET_TODOS: {
			return {
				...state,
				...payload
			};
		}
		case GET_TODO: {
			return {
				...state,
				todo: payload
			};
		}
		case UPDATE_TODO: {
			return {
				...state,
				todo: payload
			};
		}
		case ADD_TODO: {
			return {
				...state,
				data: [payload, ...state.data]
			};
		}
		case TODO_ERRORS: {
			return {
				...state,
				errors: payload
			};
		}
		case CLEAR_ERRORS: {
			return {
				...state,
				todo: null,
				errors: []
			};
		}
		default:
			return state;
	}
}
