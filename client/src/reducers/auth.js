import { LOGIN_SUCCESS, USER_LOADED, LOGIN_FAIL, LOGOUT, CLEAR_ERRORS } from "../actions/types";

const initialState = {
	token: localStorage.getItem("token"),
	isAuthenticated: null,
	user: null,
	errors: []
};

export default function(state = initialState, action) {
	const { type, payload } = action;

	switch (type) {
		case USER_LOADED:
			return {
				...state,
				isAuthenticated: true,
				user: payload
			};
		case LOGIN_SUCCESS:
			localStorage.setItem("token", payload.token);
			return {
				...state,
				...payload,
				isAuthenticated: true
			};
		case LOGIN_FAIL:
		case LOGOUT:
			localStorage.removeItem("token");
			return {
				...state,
				token: null,
				isAuthenticated: false,
				errors: payload || []
			};
		// case LOGIN_FAIL: {
		// 	return {
		// 		...state,
		// 		errors: payload
		// 	};
		// }
		case CLEAR_ERRORS: {
			return {
				...state,
				errors: []
			};
		}
		default:
			return state;
	}
}
