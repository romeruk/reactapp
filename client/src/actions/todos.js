import axios from "axios";
import { GET_TODOS, ADD_TODO, TODO_ERRORS, GET_TODO, UPDATE_TODO, CLEAR_ERRORS } from "./types";

export const getTodos = (page, filter, search) => async dispatch => {
	try {
		const result = await axios.get(`/api/todos/${page}/${filter}/${search}`);

		dispatch({
			type: GET_TODOS,
			payload: result.data
		});
	} catch (error) {
		console.log(error);
	}
};

export const addTodo = formData => async dispatch => {
	try {
		const config = {
			headers: {
				"Content-Type": "application/json"
			}
		};

		const res = await axios.post("/api/todos", formData, config);

		dispatch({
			type: ADD_TODO,
			payload: res.data
		});

		alert('Додано нову задачу');

	} catch (err) {
		dispatch({
			type: TODO_ERRORS,
			payload: err.response.data.errors
		});
	}
};

export const getTodo = (id) => async dispatch => {
	try {
		const result = await axios.get(`/api/todo/${id}`);

		dispatch({
			type: GET_TODO,
			payload: result.data
		});
	} catch (error) {
		console.log(error);
	}
};


export const updateTodo = (id, formData) => async dispatch => {

	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};
	
	try {

		const res = await axios.post(`/api/todo/${id}`, formData, config);
		dispatch({
			type: UPDATE_TODO,
			payload: res.data
		});
		alert('Задачу оновлено');

	} catch (error) {
		dispatch({
			type: TODO_ERRORS,
			payload: error.response.data.errors
		});
	}
};

export const ClearErrors = () => async dispatch => {
	dispatch({
		type: CLEAR_ERRORS
	});
}
