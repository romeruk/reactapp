import axios from "axios";
import { LOGIN_SUCCESS, LOGIN_FAIL, USER_LOADED, CLEAR_ERRORS, LOGOUT } from "./types";


import setAuthToken from '../utils/setAuthToken';

//load user
export const loadUser = () => async dispatch => {

  if(localStorage.token) {
    setAuthToken(localStorage.token);
  }

  try {
    const res = await axios.get('api/user');

    dispatch({
      type: USER_LOADED,
      payload: res.data
    });

  } catch (err) {
    console.log(err);
    // dispatch({
    //   type: AUTH_ERROR
    // });
  }
}

export const login = ( email, password ) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  const body = JSON.stringify({email, password});

  try {
    const res= await axios.post('/api/login', body, config);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    });

    dispatch(loadUser());
  }
  catch(err) {
		dispatch({
			type: LOGIN_FAIL,
			payload: err.response.data.errors
		});
  }
}

export const ClearErrors = () => async dispatch => {
	dispatch({
		type: CLEAR_ERRORS
	});
}

export const logout = () => dispatch => {
  dispatch({
    type: LOGOUT
  });
}