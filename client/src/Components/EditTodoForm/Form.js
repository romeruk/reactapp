import React, { useEffect, useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Button } from "react-bootstrap";
import { Redirect } from 'react-router-dom';
import Erros from "../Layout/Errors";
import { getTodo,updateTodo, ClearErrors } from "../../actions/todos";

const EditTodoForm = ({ match }) => {
	const dispatch = useDispatch();
	const errors = useSelector(state => state.todos.errors);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const [values, setValues] = useState({ done: false, text: "" });
	const todo = useSelector(state => state.todos.todo);

	const handleInputChange = e => {
		const { name, value } = e.target;
		setValues({ ...values, [name]: value });
	};

	const handleSubmit = e => {
		e.preventDefault();
		dispatch(updateTodo(match.params.id, values));
	};

	const loaddata = useCallback(() => {
		dispatch(getTodo(match.params.id));
	}, [dispatch, match.params.id])

	useEffect(() => {
	
		loaddata();

		return () => {
			dispatch(ClearErrors());
		}

	}, [loaddata, dispatch]);

	useEffect(() => {
		if (todo !== null) {
			console.log("Render");
			setValues({
				done: todo.done,
				text: todo.text
			});
		}
	}, [todo]);

	if(!isAuthenticated) {
    return <Redirect to="/" />
	}
	
	return (
		<div className='col-md-12 mt-5'>
			<Erros errors={errors} />

			<Form onSubmit={handleSubmit}>
				<Form.Group controlId='text'>
					<Form.Label>Todo text</Form.Label>
					<Form.Control
						onChange={handleInputChange}
						value={values.text}
						as='textarea'
						rows='3'
						name='text'
					/>
				</Form.Group>
				<Form.Group controlId='formBasicCheckbox'>
					<Form.Check
						type='checkbox'
						label='Done'
						name='done'
						value={values.done}
						checked={values.done}
						onChange={() => setValues(prev => {
							return {
								...prev,
								done: !prev.done
							}
						})}
					/>
				</Form.Group>
				<Button variant='primary' type='submit'>
					Submit
				</Button>
			</Form>
		</div>
	);
};

export default EditTodoForm;
