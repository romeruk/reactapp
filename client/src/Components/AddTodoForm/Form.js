import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Button } from "react-bootstrap";
import Erros from "../Layout/Errors";
import { addTodo, ClearErrors } from "../../actions/todos";

const AddTodoForm = () => {
	const dispatch = useDispatch();
	const errors = useSelector(state => state.todos.errors);
	const [values, setValues] = useState({ username: "", email: "example@gmail.com", text: "text" });

	const handleInputChange = e => {
		const { name, value } = e.target;
		setValues({ ...values, [name]: value });
	};

	const handleSubmit = e => {
		e.preventDefault();
		dispatch(addTodo(values));
	};

	useEffect(() => {
		return () => {
			dispatch(ClearErrors());
		}
	}, [dispatch]);

	return (
		<div className='col-md-12 mt-5'>
			<Erros errors={errors} />

			<Form onSubmit={handleSubmit}>
				<Form.Group controlId='username'>
					<Form.Label>User name</Form.Label>
					<Form.Control
						onChange={handleInputChange}
						value={values.username}
						name='username'
						type='text'
					/>
				</Form.Group>
				<Form.Group controlId='email'>
					<Form.Label>Email</Form.Label>
					<Form.Control
						onChange={handleInputChange}
						value={values.email}
						name='email'
						type='email'
					/>
				</Form.Group>
				<Form.Group controlId='text'>
					<Form.Label>Todo text</Form.Label>
					<Form.Control
						onChange={handleInputChange}
						value={values.text}
						as='textarea'
						rows='3'
						name='text'
					/>
				</Form.Group>
				<Button variant='primary' type='submit'>
					Submit
				</Button>
			</Form>
		</div>
	);
};

export default AddTodoForm;
