import React from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../../actions/auth";

const NavBar = () => {
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const dispatch = useDispatch();

	return (
		<>
			<Navbar bg='dark' variant='dark'>
				<Navbar.Brand href='#home'>Navbar</Navbar.Brand>
				<Nav className='mr-auto'>
					<Link className='nav-link' to='/'>
						Home
					</Link>
					<Link className='nav-link' to='/add'>
						Add new todo
					</Link>
					{isAuthenticated ? (
						<Button className='nav-link' onClick={() => dispatch(logout())}>
							Log out
						</Button>
					) : (
						<Link className='nav-link' to='/login'>
							Log In
						</Link>
					)}
				</Nav>
			</Navbar>
		</>
	);
};

export default NavBar;
