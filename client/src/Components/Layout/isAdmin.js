// import { useSelector } from "react-redux";
import React from "react";
import { useSelector } from "react-redux";
import hasPermmision from '../../utils/hasPermission';

const mapState = state => ({
	isAuthenticated: state.auth.isAuthenticated,
	user: state.auth.user
});

const Isdmin = ({ children }) => {
	const { isAuthenticated, user } = useSelector(mapState);
	return <div>{isAuthenticated && user && hasPermmision(user, ["ADMIN"]) && (children)}</div>;
};

export default Isdmin;
