import React, { useEffect, useState, useCallback } from "react";
import { Pagination } from "react-bootstrap";

const Paginate = ({ initalPage, count, pageSize, onChangePage }) => {
	const [pager, setPager] = useState({});

	const setPage = useCallback(pageNumber => {
		// get new pager object for specified page
		let PageObject = getPager(count, pageNumber, pageSize);

		if (pageNumber < 1 || pageNumber > PageObject.totalPages) {
			return;
		}

		pageNumber = Math.min(pageNumber, PageObject.totalPages);

		setPager(PageObject);

		onChangePage(pageNumber);
	}, [count, pageSize, onChangePage]);

	useEffect(() => {
		setPage(initalPage);
	}, [initalPage, setPage]);

	const getPager = (totalItems, currentPage, pageSize) => {
		// default to first page
		currentPage = currentPage || 1;

		// default page size is 10
		pageSize = pageSize || 10;

		// calculate total pages
		let totalPages = Math.ceil(totalItems / pageSize);

		let startPage, endPage;
		if (totalPages <= 10) {
			// less than 10 total pages so show all
			startPage = 1;
			endPage = totalPages;
		} else {
			// more than 10 total pages so calculate start and end pages
			if (currentPage <= 6) {
				startPage = 1;
				endPage = 10;
			} else if (currentPage + 4 >= totalPages) {
				startPage = totalPages - 9;
				endPage = totalPages;
			} else {
				startPage = currentPage - 5;
				endPage = currentPage + 4;
			}
		}

		// calculate start and end item indexes
		let startIndex = (currentPage - 1) * pageSize;
		let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

		// create an array of pages to ng-repeat in the pager control
		let pages = [...Array(endPage + 1 - startPage).keys()].map(i => startPage + i);

		// return object with all pager properties required by the view
		return {
			totalItems: totalItems,
			currentPage: currentPage,
			pageSize: pageSize,
			totalPages: totalPages,
			startPage: startPage,
			endPage: endPage,
			startIndex: startIndex,
			endIndex: endIndex,
			pages: pages
		};
	};

	return (
		<Pagination>
			<Pagination.First onClick={() => setPage(1)} />
			<Pagination.Prev onClick={() => setPage(pager.currentPage - 1)} />
			{pager.pages &&
				pager.pages.map((page, index) => (
					<Pagination.Item
						key={index}
						active={pager.currentPage === page}
						onClick={() => setPage(page)}
					>
						{page}
					</Pagination.Item>
				))}

			<Pagination.Next onClick={() => setPage(pager.currentPage + 1)} />
			<Pagination.Last onClick={() => setPage(pager.totalPages)} />
		</Pagination>
	);
};

export default Paginate;
