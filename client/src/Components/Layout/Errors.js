import React from 'react'

const Errors = ({ errors }) => errors !== null && errors.length > 0 && errors.map(error => (
  <div key={error.param} className={"alert alert-danger"}>
    {error.msg}
  </div>
))

export default Errors
