import React, { useEffect, useState } from "react";
import TodoHeader from "./TodoHeader";
import TodoItem from "./TodoItem";
import Paginate from "../Layout/Pagination";
import { useSelector, useDispatch } from "react-redux";
import { getTodos } from "../../actions/todos";

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this,
			args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

const TodoList = () => {
	const dispatch = useDispatch();
	const todos = useSelector(state => state.todos.data);
	const count = useSelector(state => state.todos.count);
	const pageSize = useSelector(state => state.todos.pageSize);
	const [page, setPage] = useState(2);
	const [filter, setFilter] = useState("All");
	const [search, setSearch] = useState("");

	const handleChangePage = currentPage => {
		setPage(currentPage);
	};

	const handleFilter = type => {
		setPage(1);
		setFilter(type);
	};

	const handleSearch = debounce(val => {
		setSearch(val);
	}, 1000);

	useEffect(() => {
		dispatch(getTodos(page, filter, search));
	}, [dispatch, page, filter, search]);

	return (
		<div className='col-md-12 mt-5'>
			<TodoHeader filter={filter} handleFilter={handleFilter} handleSearch={handleSearch} />
			<ul className='list-unstyled'>
				{todos.map(todo => (
					<TodoItem key={todo._id} todo={todo} />
				))}
			</ul>

			<Paginate
				onChangePage={handleChangePage}
				initalPage={page}
				count={count}
				pageSize={pageSize}
			/>
		</div>
	);
};

export default TodoList;
