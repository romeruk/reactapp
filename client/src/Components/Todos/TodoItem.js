import React from "react";
import { Media } from "react-bootstrap";
import { Link } from "react-router-dom";
import Isdmin from "../Layout/isAdmin";

const TodoItem = ({ todo: { text, username, email, done, _id } }) => {
	return (
		<Media as='li' className='mb-3'>
			<Media.Body>
				<h5>{username}</h5>
				<small>{email}</small>
				<p>{done ? <del>{text}</del> : <span>{text}}</span>}</p>
				<Isdmin>
					<Link className='btn btn-primary' to={`/todo/${_id}`}>
						Edit
					</Link>
				</Isdmin>
			</Media.Body>
		</Media>
	);
};

export default TodoItem;
