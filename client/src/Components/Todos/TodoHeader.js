import React from "react";
import { ListGroup, Form } from "react-bootstrap";

const TodoHeader = ({ filter, handleFilter, handleSearch }) => {
	return (
		<div className='row mb-5'>
			<div className='col-md-6'>
				<ListGroup as='ul'>
					<ListGroup.Item
						as='li'
						active={filter === "All"}
						action
						onClick={() => handleFilter("All")}
					>
						All
					</ListGroup.Item>
					<ListGroup.Item
						as='li'
						active={filter === "Done"}
						action
						onClick={() => handleFilter("Done")}
					>
						Done
					</ListGroup.Item>
					<ListGroup.Item
						as='li'
						active={filter === "Undone"}
						action
						onClick={() => handleFilter("Undone")}
					>
						Undone
					</ListGroup.Item>
				</ListGroup>
			</div>

			<div className='col-md-6'>
				<Form>
					<Form.Group controlId='formBasicEmail'>
						<Form.Label>Search by User Name or email</Form.Label>
						<Form.Control
							onChange={e => handleSearch(e.target.value)}
							type='text'
							placeholder='Enter email'
						/>
					</Form.Group>
				</Form>
			</div>
		</div>
	);
};

export default TodoHeader;
