import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Redirect } from 'react-router-dom';
import { Form, Button } from "react-bootstrap";
import Erros from "../Layout/Errors";
import { login, ClearErrors } from "../../actions/auth";

const LoginForm = () => {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const errors = useSelector(state => state.auth.errors);
	const [values, setValues] = useState({ password: "", email: ""});

	const handleInputChange = e => {
		const { name, value } = e.target;
		setValues({ ...values, [name]: value });
	};

	const handleSubmit = e => {
		e.preventDefault();
		dispatch(login(values.email, values.password));
	};

	useEffect(() => {
		return () => {
			dispatch(ClearErrors());
		}
	}, [dispatch]);

  if(isAuthenticated) {
    return <Redirect to="/" />
  }

	return (
		<div className='col-md-12 mt-5'>
			<Erros errors={errors} />

			<Form onSubmit={handleSubmit}>
				<Form.Group controlId='email'>
					<Form.Label>Email</Form.Label>
					<Form.Control
						onChange={handleInputChange}
						value={values.email}
						name='email'
						type='email'
					/>
				</Form.Group>
        <Form.Group controlId='password'>
					<Form.Label>Password</Form.Label>
					<Form.Control
						onChange={handleInputChange}
						value={values.password}
						name='password'
						type='password'
					/>
				</Form.Group>
				<Button variant='primary' type='submit'>
					Submit
				</Button>
			</Form>
		</div>
	);
};

export default LoginForm;