import React from "react";
import { Route, Redirect } from "react-router-dom";

import { useSelector } from "react-redux";
import hasPermmision from "../../utils/hasPermission";

const mapState = state => ({
	isAuthenticated: state.auth.isAuthenticated,
	user: state.auth.user
});

const AdminRoute = ({ component: Component, permissions, ...rest }) => {
	const { isAuthenticated, user } = useSelector(mapState);
	
	return (
		<Route
			{...rest}
			render={props =>
				(!isAuthenticated && !user) || !hasPermmision(user, permissions) ? <Redirect to='/login' /> : <Component {...props} />
			}
		/>
	);
};

export default AdminRoute;
