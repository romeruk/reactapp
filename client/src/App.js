import React, { useEffect } from "react";
// import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AdminRoute from './Components/Routing/AdminRoute';
import NavBar from "./Components/Layout/NavBar";
import TodoList from "./Components/Todos/TodoList";
import AddTodoForm from "./Components/AddTodoForm/Form";
import EditTodoForm from "./Components/EditTodoForm/Form";
import LoginForm from "./Components/Login/LoginForm";

import { Provider } from "react-redux";
import store from "./store";
import { loadUser } from "./actions/auth";
import setAuthToken from "./utils/setAuthToken";

if (localStorage.token) {
	setAuthToken(localStorage.token);
}

function App() {
	useEffect(() => {
		if (localStorage.token) store.dispatch(loadUser());
	}, []);

	return (
		<Provider store={store}>
			<Router>
				<NavBar />
				<main role='main' className='container'>
					<div className='row'>
						<Switch>
							<Route exact path='/' component={TodoList} />
							<Route exact path='/add' component={AddTodoForm} />
							<Route exact path='/login' component={LoginForm} />
							<AdminRoute exact path='/todo/:id' permissions={["admin"]} component={EditTodoForm}/>
						</Switch>
					</div>
				</main>
			</Router>
		</Provider>
	);
}

export default App;
