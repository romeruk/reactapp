function hasPermission(user, permissionsNeeded) {
  if (!user) {
    return false;
  }
  
  const matchedPermissions = user.permissions.filter(permissionTheyHave =>
    permissionsNeeded.map(v => v.toLowerCase()).includes(permissionTheyHave.toLowerCase())
  );

  if (!matchedPermissions.length) {
    return false;
  }

  return true;
}

module.exports = hasPermission;