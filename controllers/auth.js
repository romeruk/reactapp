const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const User = require("../models/user");

exports.getUser = async (req, res) => {
	try {
		const user = await User.findById(req.user.id).select("-password");

		res.json(user);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server error");
	}
};

exports.login = async (req, res) => {
	await check("email", "Поле 'email' повинно бути типу email")
		.isEmail()
		.run(req);
	await check("password", "Поле 'password' обов'язкове до заповнення")
		.not()
		.isEmpty()
		.run(req);
	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		return res.status(400).json({
			errors: errors.array()
		});
	}

	const { password } = req.body;
	let email = req.body.email.toLowerCase();
	
	try {
		// See if user exists
		let user = await User.findOne({ email });

		if (!user) {
			return res.status(400).json({ errors: [{ msg: "Invalid Credentials" }] });
		}

		//Compare password
		const isMatch = await bcrypt.compare(password, user.password);

		if (!isMatch) {
			return res.status(400).json({ errors: [{ msg: "Invalid Credentials" }] });
		}

		const payload = {
			user: {
				id: user.id
			}
		};

		jwt.sign(
			payload,
			config.get("jwtSecret"),
			{
				expiresIn: 36000
			},
			(err, token) => {
				if (err) throw err;
				res.json({ token });
			}
		);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server error");
	}
};
