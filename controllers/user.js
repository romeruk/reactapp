// const bcrypt = require("bcryptjs");
// const jwt = require("jsonwebtoken");
// const config = require("config");

// const User = require("../models/user");

// //@route  POST api/users
// //@desc   Register user
// //@access Public
// exports.register = async (req, res) => {
// 	const { username, email, password } = req.body;

// 	try {
// 		// See if user exists
// 		let user = await User.findOne({ email });

// 		if (user) {
// 			return res.status(400).json({ errors: [{ msg: "User already exists" }] });
// 		}

// 		user = new User({
// 			username,
// 			email,
// 			password
// 		});

// 		// Encrypt password
// 		const salt = await bcrypt.genSalt(10);

// 		user.password = await bcrypt.hash(password, salt);

// 		await user.save();

// 		res.json(user);

// 		// jwt.sign(payload, config.get('jwtSecret'), {
// 		//   expiresIn: 36000
// 		// },
// 		// (err, token) => {
// 		//   if(err) throw err
// 		//   res.json({token});
// 		// });
// 	} catch (err) {
// 		console.error(err.message);
// 		res.status(500).send("Server error");
// 	}
// };