const Todos = require("../models/todo");
const { check, validationResult, body } = require("express-validator");

function escapeRegex(text) {
	return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

exports.getTodos = async (req, res, next) => {
	let page = parseInt(req.params.page, 10) || 1;
	let filter = req.params.filter === "undefined" ? "All" : req.params.filter;
	let search = req.params.search;

	console.log(filter);
	const resPerPage = 3;

	const filterOptions = new Map([["All", [false, true]], ["Done", true], ["Undone", false]]);

	let query = {
		done: filterOptions.get(filter)
	};

	if (search) {
		const regex = new RegExp(escapeRegex(search), "gi");
		query.$or = [{ username: regex }, { email: regex }];
	}

	console.log(query);

	try {
		const todos = await Todos.find(query)
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		const count = await Todos.countDocuments(query);

		res.json({
			data: todos,
			pageSize: resPerPage,
			count
		});
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server error");
	}
};

exports.postTodo = async (req, res, next) => {
	try {
		await check("username")
			.not()
			.isEmpty()
			.withMessage("Поле 'User Name' обов'язкове до заповнення")
			.run(req);
		await check("text")
			.not()
			.isEmpty()
			.withMessage("Поле 'Text' обов'язкове до заповнення")
			.run(req);
		await check("email")
			.isEmail()
			.withMessage("Поле 'email' повинно бути типу email")
			.run(req);

		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array()
			});
		}

		const { text, email, username } = req.body;

		const createTodo = new Todos({
			text,
			email,
			username
		});

		const todo = await createTodo.save();
		res.json(todo);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server error");
	}
};

exports.getTodo = async (req, res, next) => {
	try {
		const todo = await Todos.findById(req.params.id);

		if (!todo) {
			return res.status(404).json({ msg: "todo not found" });
		}

		res.json(todo);
	} catch (err) {
		console.error(err.message);
		if (err.kind === "ObjectId") {
			return res.status(404).json({ msg: "todo not found" });
		}
		res.status(500).send("Server error");
	}
};

exports.editTodo = async (req, res) => {
	try {
		await check("text")
			.not()
			.isEmpty()
			.withMessage("Поле 'Text' обов'язкове до заповнення")
			.run(req);

		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array()
			});
		}
		const { text, done } = req.body;

		const todo = await Todos.findById(req.params.id);

		if (!todo) {
			return res.status(404).json({ msg: "todo not found" });
		}

		todo.text = text;
		todo.done = done;

		const uptTodo = await todo.save();

		res.json(uptTodo);
	} catch (err) {
		console.error(err.message);
		if (err.kind === "ObjectId") {
			return res.status(404).json({ msg: "todo not found" });
		}
		res.status(500).send("Server error");
	}
};
