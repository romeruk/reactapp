const express = require("express");
const connectDB = require("./config/db");
const path = require("path");
const app = express();

//connectDB
connectDB();

const PORT = process.env.PORT || 5000;

app.use(express.json({ extended: false }));

const todoRoutes = require("./routes/todos");
const authRoutes = require("./routes/auth");

app.use(todoRoutes);
app.use(authRoutes);

if (process.env.NODE_ENV === "production") {
	app.use(express.static(path.join(__dirname, "client/build")));
	//
	app.get("*", (req, res) => {
		res.sendfile(path.join((__dirname = "client/build/index.html")));
	});
}

//Route Operations...
app.get("/", (req, res) => {
	res.send("Root route of server");
});

app.listen(PORT, () => {
	console.log(`Server started at port ${PORT}`);
});
